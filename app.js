let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let session = require('express-session');
let cors = require('cors');
let passport = require('passport');
let mongoose = require('mongoose');
const port = 3000;

mongoose.connect('mongodb://AlexTi:Qwert1234@ds155268.mlab.com:55268/blogsapp_db');

//Application initizlize
let app = express();
let router = express.Router();

//Views Engine
//---No views engine---

//Static folder settings
//---No public folder---

//CORS Middleware
const corsOptions = {
    	origin: 'http://localhost:8080',
    	credentials: true
} 
app.use(cors(corsOptions));

//Bodyparser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

//Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

//Passport initizlise
app.use(passport.initialize());
app.use(passport.session());

//Global Vars
app.use((req, res, next) => {
	res.locals.user = req.user || null;
	next();
});

//Index page render
app.get('/', (req, res, next) => {
	res.send({
		success: true,
		message: 'Welcome to Blogs application'
	});
});

app.use('/blogs', require('./routes/blogs'));

//Time page
app.get('/time', (req, res) => {
	res.send((new Date()).toLocaleTimeString());
});

//Default page render if no matches
app.use((req, res, next) => {
	res.send({
		success: false,
		message: 'No such page! =('
	});
	next();
});

//Error handling middleware
app.use((error, req, res, next) => {
	console.error(error);
	res.status(error.status || 500);
	res.send({
		success: false,
		message: 'Error!' + error.message
	});
});

//Start listen to application
app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }

    console.log(`server is listening on ${port}`)
});