let express = require('express');
let router = express.Router();
let bodyParser = require('body-parser');
let mongoose = require('mongoose');

const Blog = require('../models/Blog');

//All blogs page
function getBlogs(req, res) {
	Blog.find({}, (error, blogs) => {
		if(!blogs) next(new Error('No blogs found :('));
		if (!error) {
			res.send({
				type: 'blogs_get',
				success: true,
				blogs: blogs
			})
		} else {
			next(error);
		}
	});
}
router.get('/', getBlogs);

//Particular blog page
function getBlog(req, res, next) {
	const id = req.params.id;

	Blog.findById(id, (error, blog) => {
		if (!error) {
			res.send({
				type: 'blog_get',
				success: true,
				blog: blog
			})
		} else {
			res.send(error);
		}
	});
}
router.get('/:id', getBlog);

//Add new blog
function addBlog(req, res, next) {
	const blog = new Blog({
		title: req.body.title,
		author: req.body.author,
		date: (new Date().toISOString()),
		description: req.body.description
	});

	blog.save((err, blog) => {
		if(err) {
			res.send(err);
		} else {
			res.send({
				type: 'blog_add',
				success: true,
				blog,
				message: 'Blog has been added!'
			});
		}
	});
}
router.put('/', addBlog);

//Update existing blog's message
function updateBlog(req, res, next) {
	const id = req.body.id;
	const changedBlog = {
		title: req.body.title,
		author: req.body.author,
		date: (new Date().toISOString()),
		description: req.body.description
	};

	Blog.findByIdAndUpdate(id, changedBlog, (error, blog) => {
		if (!error) {
			const retBlog = changedBlog;
			retBlog._id = id;
			res.send({
				type: 'blog_update',
				success: true,
				blog: retBlog,
				message: 'Blog has beeen updated!'
			});
		} else {
			next(error);
		}
	});
}
router.post('/update', updateBlog);

//Delete existing blog
function deleteBlog(req, res, next) {
	const id = req.params.id;

	Blog.findById(id).remove(() => {
		res.send({
			type: 'blog_delete',
			success: true,
			message: 'Blog has beeen deleted!'
		});
	});
}
router.delete('/:id', deleteBlog);

module.exports = router;